import { MutableSequence, Sequence } from "./interfaces";

export function contains<T>(sequence: Sequence<T>, value: T): boolean {
  for (let v of sequence) {
    if (v === value) {
      return true;
    }
  }
  return false;
}

export function* reversed<T>(sequence: Sequence<T>): Iterable<T> {
  for (let idx = sequence.length() - 1; idx >= 0; idx--) {
    yield sequence.at(idx);
  }
}

export function indexOf<T>(
  sequence: Sequence<T>,
  value: T,
  fromIndex?: number
): number {
  if (sequence.indexOf) {
    return sequence.indexOf(value, fromIndex);
  } else {
    let start = 0;
    let length = sequence.length();
    const itr = sequence[Symbol.iterator]();
    let idx = 0;

    if (fromIndex !== undefined) {
      if (fromIndex >= length) {
        return -1;
      } else if (fromIndex >= 0) {
        start = fromIndex;
      } else if (fromIndex >= -length) {
        start = fromIndex + length;
      }
    }

    for (idx = 0; idx < start; idx++) {
      itr.next();
    }

    while (true) {
      const { done, value: val } = itr.next();
      if (!done) {
        if (val === value) {
          return idx;
        } else {
          idx += 1;
        }
      } else {
        return -1;
      }
    }
  }
}

export function count<T>(sequence: Sequence<T>, value: T): number {
  if (sequence.count) {
    return sequence.count(value);
  } else {
    let count = 0;

    for (let v of sequence) {
      if (v === value) {
        count += 1;
      }
    }
    return count;
  }
}

export function append<T>(sequence: MutableSequence<T>, value: T): void {
  if (sequence.append) {
    sequence.append(value);
  } else {
    sequence.insert(sequence.length(), value);
  }
}

export function reverse<T>(sequence: MutableSequence<T>): void {
  if (sequence.reverse) {
    sequence.reverse();
  } else {
    const n = sequence.length();
    for (let idx = 0; idx < Math.floor(n / 2); idx++) {
      const tmp = sequence.at(n - idx - 1);
      sequence.setAt(n - idx - 1, sequence.at(idx));
      sequence.setAt(idx, tmp);
    }
  }
}

export function extend<T>(
  sequence: MutableSequence<T>,
  iterable: Iterable<T>
): void {
  if (sequence.extend) {
    sequence.extend(iterable);
  } else {
    let itr = iterable;

    if (Object.is(itr, sequence)) {
      itr = Array.from(iterable);
    }
    if (sequence.append) {
      for (let value of itr) {
        sequence.append(value);
      }
    } else {
      for (let value of itr) {
        append(sequence, value);
      }
    }
  }
}

export function pop<T>(sequence: MutableSequence<T>, index: number = -1): T {
  if (sequence.pop) {
    return sequence.pop(index);
  } else {
    const value = sequence.at(index);
    sequence.delAt(index);
    return value;
  }
}

export function remove<T>(sequence: MutableSequence<T>, value: T): void {
  if (sequence.remove) {
    sequence;
  } else {
    let idx = -1;

    if (sequence.indexOf) {
      idx = sequence.indexOf(value);
    } else {
      idx = indexOf(sequence, value);
    }
    if (idx >= 0) {
      sequence.delAt(idx);
    }
  }
}
