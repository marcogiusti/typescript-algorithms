import { MutableSequence } from "../interfaces";
import {
  append,
  contains,
  count,
  extend,
  indexOf,
  pop,
  remove,
  reverse,
  reversed,
} from "../generic_functions";
import { IndexError } from "../exceptions";

class List<T> implements MutableSequence<T> {
  _array: Array<T>;

  constructor() {
    this._array = new Array<T>();
  }

  contains(value: T): boolean {
    return contains(this, value);
  }

  reversed(): Iterable<T> {
    return reversed(this);
  }

  length(): number {
    return this._array.length;
  }

  [Symbol.iterator](): IterableIterator<T> {
    return this._array[Symbol.iterator]();
  }

  at(index: number): T {
    if (index < -this._array.length || index >= this._array.length) {
      throw new IndexError("index out of range");
    }
    return this._array.at(index)!;
  }

  setAt(index: number, value: T): void {
    this._array[index] = value;
  }

  delAt(index: number): void {
    this._array.splice(index, 1);
  }

  insert(index: number, value: T): void {
    this._array.splice(index, 0, value);
  }
}

describe("Generic functions", () => {
  describe("Container", () => {
    test("contains", () => {
      const list = new List<number>();
      expect(contains(list, 1)).toBe(false);
      list.insert(0, 1);
      expect(contains(list, 1)).toBe(true);
      expect(contains(list, 2)).toBe(false);
    });
  });

  describe("Reversible", () => {
    test("reversed", () => {
      const list = new List<number>();
      list.insert(0, 1);
      list.insert(1, 2);
      expect(Array.from(list)).toEqual([1, 2]);
      expect(Array.from(reversed(list))).toEqual([2, 1]);
    });
  });

  describe("Sequence", () => {
    test("indexOf", () => {
      const list = new List<number>();
      expect(indexOf(list, 1)).toEqual(-1);
      list.insert(0, 1);
      expect(indexOf(list, 1)).toEqual(0);
      list.insert(1, 2);
      expect(indexOf(list, 2)).toEqual(1);
    });

    test("count", () => {
      const list = new List<number>();
      expect(count(list, 1)).toEqual(0);
      list.insert(0, 1);
      list.insert(1, 1);
      expect(count(list, 1)).toEqual(2);
      expect(count(list, 2)).toEqual(0);
    });
  });

  describe("MutableSequence", () => {
    test("append", () => {
      const list = new List<number>();
      expect(count(list, 1)).toEqual(0);
      append(list, 1);
      append(list, 2);
      expect(Array.from(list)).toEqual([1, 2]);
    });

    test("reverse", () => {
      const list = new List<number>();
      expect(count(list, 1)).toEqual(0);
      list.insert(0, 1);
      list.insert(1, 2);
      list.insert(2, 3);
      expect(Array.from(list)).toEqual([1, 2, 3]);
      reverse(list);
      expect(Array.from(list)).toEqual([3, 2, 1]);
    });

    test("extend", () => {
      const list = new List<number>();
      extend(list, [1, 2]);
      expect(Array.from(list)).toEqual([1, 2]);
      extend(list, list);
      expect(Array.from(list)).toEqual([1, 2, 1, 2]);
      // Not supported case
      // extend(list, list[Symbol.iterator]());
    });

    test("pop", () => {
      const list = new List<number>();
      list.insert(0, 1);
      list.insert(1, 2);
      expect(Array.from(list)).toEqual([1, 2]);
      let val = pop(list);
      expect(val).toEqual(2);
      expect(Array.from(list)).toEqual([1]);
      val = pop(list);
      expect(val).toEqual(1);
      expect(Array.from(list)).toEqual([]);
      expect(() => pop(list)).toThrow(IndexError);
      list.insert(0, 1);
      list.insert(1, 2);
      expect(() => pop(list, 2)).toThrow(IndexError);
      expect(() => pop(list, -3)).toThrow(IndexError);
      val = pop(list, 0);
      expect(val).toEqual(1);
      expect(Array.from(list)).toEqual([2]);
    });

    test("remove", () => {
      const list = new List<number>();
      list.insert(0, 1);
      list.insert(1, 2);
      list.insert(2, 1);
      expect(Array.from(list)).toEqual([1, 2, 1]);
      remove(list, 1);
      expect(Array.from(list)).toEqual([2, 1]);
      remove(list, 3);
      expect(Array.from(list)).toEqual([2, 1]);
    });
  });
});
