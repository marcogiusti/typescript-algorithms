export interface Container<T> {
  contains(value: T): boolean;
}

export interface Reversible<T> extends Iterable<T> {
  reversed(): Iterable<T>;
}

export interface Sized {
  length(): number;
}

export interface Collection<T> extends Sized, Iterable<T>, Container<T> {}

export interface Sequence<T> extends Reversible<T>, Collection<T> {
  at(index: number): T;
  indexOf?(value: T, fromIndex?: number): number;
  count?(value: T): number;
}

export interface MutableSequence<T> extends Sequence<T> {
  setAt(index: number, value: T): void;
  delAt(index: number): void;
  insert(index: number, value: T): void;
  append?(value: T): void;
  reverse?(): void;
  extend?(iterable: Iterable<T>): void;
  pop?(index: number): T;
  remove?(value: T): void;
}
